

fabric.Block = fabric.util.createClass(fabric.Rect, {

	type: 'block',

	initialize: function(element, options) {
		options || (options = {});

		element.fill = 'transparent';
		element.stroke = 'black';
		element.strokeWidth = 1;
		//element.hasBorders = false;
		//element.hasControls = false;
		//element.lockMovementX = true; 
		//element.lockMovementY = true;

		element.data = !(typeof(element.data) === 'string' || element.data instanceof String) ? element.data : BinaryArray(this.data);	

		this.callSuper('initialize', element, options);
		
		this.bottom = element.top  + element.height;
		this.right  = element.left + element.width;
		this.center = element.left + element.width/2.0;
		this.middle = element.top  + element.height/2.0;
		this.hex = element.data.toHexString();
	},

	toObject: function() {
		return fabric.util.object.extend(this.callSuper('toObject'));
	},

	_render: function(ctx) {
		this.callSuper('_render', ctx);

		// do not render if width/height are zeros or object is not visible
		if ((this.width === 0 && this.height === 0) || !this.visible) return;

		ctx.save();

		var hex = this.hex;

		var cancelTitle = this.name != ' ' ? 0 : -15;
		

		ctx.font = "12px Consolas";
		ctx.fillStyle = "black";
		ctx.textAlign = "center";
		ctx.fillText(this.data.text, 0, 10 + cancelTitle);
		ctx.fillText(hex, 0, 25 + cancelTitle);

		if (this.name != ' ')
		{
			ctx.font = "12px Serif";
			ctx.fillText(this.name, 0, -10);
			cancelTitle = 0;
		}

		ctx.textAlign = "left";
		ctx.font = "12px Consolas";
		ctx.fillText('(' + this.data.length + ')', this.width/2-30, this.height/2-5);

		ctx.restore();

	}
});

fabric.FBlock = fabric.util.createClass(fabric.Rect, {

	type: 'block',

	initialize: function(element, options) {
		options || (options = {});

		element.fill = 'transparent';
		element.stroke = 'black';
		element.strokeWidth = 1;
		element.hasBorders = false;
		element.hasControls = false;
		element.lockMovementX = true; 
		element.lockMovementY = true;

		this.callSuper('initialize', element, options);
		
		this.bottom = element.top  + element.height;
		this.right  = element.left + element.width;
		this.center = element.left + element.width/2.0;
		this.middle = element.top  + element.height/2.0;
	},

	toObject: function() {
		return fabric.util.object.extend(this.callSuper('toObject'));
	},

	_render: function(ctx) {
		this.callSuper('_render', ctx);

		// do not render if width/height are zeros or object is not visible
		if ((this.width === 0 && this.height === 0) || !this.visible) return;

		ctx.save();

		ctx.fillStyle = "black";
		ctx.textAlign = "center";
		ctx.font = "18px Serif";
		ctx.fillText(this.name, 0, 5);

		ctx.restore();

	}
});


fabric.LineArrow = fabric.util.createClass(fabric.Line, {

	type: 'lineArrow',

	initialize: function(element, options) {
		options || (options = {});
		this.callSuper('initialize', element, options);
	},

	toObject: function() {
		return fabric.util.object.extend(this.callSuper('toObject'));
	},

	_render: function(ctx) {
		this.callSuper('_render', ctx);

		// do not render if width/height are zeros or object is not visible
		if ((this.width === 0 && this.height === 0) || !this.visible) return;

		ctx.save();

		var xDiff = this.x2 - this.x1;
		var yDiff = this.y2 - this.y1;
		var angle = Math.atan2(yDiff, xDiff);
		ctx.translate((this.x2 - this.x1) / 2, (this.y2 - this.y1) / 2);
		ctx.rotate(angle);
		ctx.beginPath();
		//move 10px in front of line to start the arrow so it does not have the square line end showing in front (0,0)
		ctx.moveTo(0, 0);
		ctx.lineTo(-8, 3);
		ctx.lineTo(-8, -3);
		ctx.closePath();
		ctx.fillStyle = this.stroke;
		ctx.fill();

		ctx.restore();

	}
});


fabric.XorGate = fabric.util.createClass(fabric.Circle, {

	type: 'xorGate',

	initialize: function(element, options) {
		options || (options = {});

		element.stroke = 'black';
		element.strokeWidth = 1;
		element.fill = 'transparent';
		element.hasBorders = false;
		element.hasControls = false;
		element.lockMovementX = true;
		element.lockMovementY = true;

		this.callSuper('initialize', element, options);

		this.bottom = element.top  + element.radius * 2;
		this.right  = element.left + element.radius * 2;
		this.center = element.left + element.radius;
		this.middle = element.top  + element.radius;
	},

	toObject: function() {
		return fabric.util.object.extend(this.callSuper('toObject'));
	},

	_render: function(ctx) {
		this.callSuper('_render', ctx);

		// do not render if width/height are zeros or object is not visible
		if ((this.width === 0 && this.height === 0) || !this.visible) return;

		ctx.save();
		ctx.beginPath();
		ctx.moveTo(-this.radius, 0);
		ctx.lineTo(this.radius,0);
		ctx.moveTo(0, -this.radius);
		ctx.lineTo(0, this.radius);
		ctx.stroke();

		
		
		var xDiff = this.x2 - this.x1;
		var yDiff = this.y2 - this.y1;
		var angle = Math.atan2(yDiff, xDiff);
		ctx.translate((this.x2 - this.x1) / 2, (this.y2 - this.y1) / 2);
		ctx.rotate(angle);
		//move 10px in front of line to start the arrow so it does not have the square line end showing in front (0,0)
		

		ctx.restore();

	}
});


