

var BinaryArrayCore = function(t){

	t = (typeof(t) === 'string' || t instanceof String) ? t : t.text;

	return {
		text: t,
		length: t.length,
		xor: function(r) {
			var str = Array.from(this.text).map((x,i) => x != r.text.charAt(i) ? '1' : '0').reduce((x,y) => x+y, '');
			return BinaryArray(str);
		},
		permutate: function(indexes) {
			var str = indexes.map((x,i) => t.charAt(x)).reduce((x,y) => x+y, '');
			return BinaryArray(str);
		},
		rotateLeft: function(count) {
			var str = t.substr(count) + t.substr(0, count);
			return BinaryArray(str);
		},
		rotateRight: function(count) {
			var str = t.substr(t.length - count) + t.substr(0, t.length - count);
			return BinaryArray(str);
		},
		firstHalf: function() {
			var str = t.substr(0, t.length/2);
			return BinaryArray(str);
		},
		secondHalf: function() {
			var str = t.substr(t.length/2);
			return BinaryArray(str);
		},
		split: function(length){
			var arr = Array(Math.round(t.length/length)).fill(null).map((x,i) => BinaryArray(t.substr(i * length, length)));
			return arr;
		},
		merge: function(r) {
			var str = t + r.text;
			return BinaryArray(str);
		},
		take: function(length){
			var str = t.substr(0, length);
			return BinaryArray(str);
		},
		skipTake: function(skip, length) {
			var str = t.substr(skip, length);
			return BinaryArray(str);
		},
		insert: function(position, r) {
			var str = t.substr(0, position) + r.text + t.substr(position);
			return BinaryArray(str);
		},
		toInt: function() {
			var val = Array.from(t).map((x,i) => parseInt(x) * Math.pow(2, t.length - 1- i)).reduce((x,y) => x+y, 0);
			return val;
		},


		toByteArray: function(){
			var array = Array(this.text.length/8).fill(null).map((x,i) => parseInt(this.text.substr(i*8, 8), 2));
			return array;
		},
		toUtf8: function(){
			var array = this.toByteArray();
			return decodeURIComponent(array.reduce((p,c)=>{return p+'%'+c.toString(16)}, ''));
		},
		toAscii: function(){
			var array = this.toByteArray();
			return array.reduce((p,c) => { return p + String.fromCharCode(c) }, '');
		},
		toBase64: function(){
		 	var array = this.toByteArray();
			var buffer =  Uint8Array.from(array);
			var base64 = btoa(array.reduce((data, byte) => data + String.fromCharCode(byte), ''))
			return base64;
		},
		toHexString: function(){
			//var array = this.toByteArray();
			var array = Array(this.text.length/4).fill(null).map((x,i) => parseInt(this.text.substr(i*4, 4), 2));
			var hex = array.map(x => x.toString(16).padStart(1, '0')).reduce((x,y) => x + y, '');
			return hex.toUpperCase();
		},
		toBinaryString: function(){
			return this.text;
		},
		toString: function(type){
			if (type == "binary")      { return this.toBinaryString(); }
			else if (type == "hex")    { return this.toHexString(); }
			else if (type == "base64") { return this.toBase64(); }
			else if (type == "ascii")  { return this.toAscii(); }
			else if (type == "utf8")   { return this.toUtf8(); }
			return this.toBinaryString();
		}
	};
}


var BinaryArray = function(t, type) {
	
	if (t instanceof Array){
		var binaryText = t.reduce((p,c) => { return p + c.toString(2).padStart(8, '0' ) }, '');
		return BinaryArrayCore(binaryText); 
	}

	if (type == "binary" || type == null) {
		return BinaryArrayCore(t);
	}
	else if (type == "hex") {
		var array = Array(t.length/2).fill(null).map((x,i) => parseInt(t.substr(i*2, 2), 16)); 
		var binaryText = array.reduce((p,c) => { return p + c.toString(2).padStart(8, '0' ) }, '');
		return BinaryArrayCore(binaryText);
	}
	else if (type == 'utf8') {
		var bytes = Uint8Array.from(encodeURIComponent(t).replace(/%(..)/g,(m,v)=>{return String.fromCodePoint(parseInt(v,16))}), c=>c.codePointAt(0));
		var array = Array.from(bytes);
		var binaryText = array.reduce((p,c) => { return p + c.toString(2).padStart(8, '0' ) }, '');
		return BinaryArrayCore(binaryText);
	}
	else if (type == 'ascii') {
		var bytes = t.split('').map(x => x.charCodeAt(0));
		var error = bytes.reduce((p,c) => (c > 255) | p, false);
		if (error) { throw 'Hatalı eleman mevcut.'; }
		var binaryText = bytes.map(x => x.toString(2).padStart(8, '0')).reduce((x,y) => x + y, '');
		return BinaryArrayCore(binaryText);
	}
	else if (type == 'base64') {
		var bytes = atob(t);
		var binaryText = bytes.split('').map(x => x.charCodeAt(0).toString(2).padStart(8, '0')).reduce((x,y) => x + y, '');
		return BinaryArrayCore(binaryText);
		
	}
}


var BinaryArrayFromInt = function(value, length){
	var str = value.toString(2);
	if (length != null)
	{
		if (value.toString(2).length > length)
			str = str.substr(value.toString(2).length-length, length);
		str = str.padStart(length, '0');
	}
	return BinaryArray(str, 'binary');
}
