var Polynomial = function(data, symbol) {
	

	return  {
		data: data,
		length: data.length,

		calculate: function(value) {
			var result = data.map((x,i) => x * (value**i)).reduce((a,x) => a+x, 0);
			return result;
		},
		add: function(polynomial) {
			
			if(isNumeric(polynomial)){

				var result = data.map(x => x + polynomial);
			}
			else {
				var biggerPolynom = data.length > polynomial.length ? this : polynomial;
				var lowerPolynom = data.length <= polynomial.length ? this : polynomial;
				var diff = biggerPolynom.length - lowerPolynom.length;

				var result = Array(biggerPolynom.length);

				for (var i = result.length-1; i >= 0; i--) {
					var indexLower = i - diff;
					if (indexLower < 0)
						result[i] = biggerPolynom.data[i];
					else
						result[i] = biggerPolynom.data[i] + lowerPolynom.data[indexLower];
				}
			}
			return Polynomial(result, symbol);
		},
		multiply: function(polynomial) {

			if(isNumeric(polynomial)){

				var result = data.map(x => x * polynomial);
			}
			else {
				var result = Array(this.length + polynomial.length - 1).fill(0);
				for(var i = 0; i < this.length; i++) {
					var power_i = this.length - i - 1; 
					for (var j = 0;  j < polynomial.length; j++){
						var power_j = polynomial.length - j - 1;
						var power = power_i + power_j;
						var index = result.length - power - 1;
						result[index] += this.data[i] * polynomial.data[j];
					}
				}
			}

			return Polynomial(result, symbol);
		},
		toLatex: function(symb) {

			text = '';

			for (var i = 0; i < data.length; i++) {
				symb = symbol;

				if (data[i] == 0)
					continue;

				var sign = data[i] == 0 | (i == 0 & data[i] >= 0) ? '' : (data[i] < 0 ? '-' : '+');
				var smbl = i == (data.length-1) ? '' : symb + (i != data.length-2 ? '^' + (data.length - i - 1) : '');
				var value = (data[i] != 1 && data[i] != -1) || (i == data.length-1) ? Math.abs(data[i]) : '';
				text += sign + value + smbl;
			}
			return text;
		},
		clone: function(){
			var result =  Polynomial(data.map(x => x), symbol);
			return result;
		},
	}
}



function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}