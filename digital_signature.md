## Digtal Signature

```plantuml
@startuml
scale 1.2
Alice -> Bob : <latex>(n, e)</latex>
note left: <latex>\\text{K}_{\\text{prA}} = d</latex> \n <latex>\\text{K}_{\\text{pubA}} = (n, e)</latex>

Alice -> Bob : <latex>(x, s)</latex>
note left: <latex>\\text{s} = \\text{sig}_{\\text{K}_{\\text{prA}}}(x) \\equiv x^d \\, (\\text{mod} \\, n) </latex> 

Bob -> Bob: Check is valid.
note right 
    <latex>\text{ver}_{\text{K}_{\text{pubA}}}(x, s)</latex>
    <latex>s^e = x' \, (\text{mod} \, n)</latex> 
    <latex>r = (x'\stackrel{?}{=} x) \text{ ? } \text{valid} : \text{invalid}</latex>
end note

@enduml
```

RSA algoritması kullanılarak dijital imza oluşturulabilir. RSA'de şifrelencek metin alıcının public keyi ile şifrelenir ancak burada göndericinin privete keyi ile şifrelenir ve göndericinin public keyi ile şifre çözülerek göndericinin kimliği kontrol edilir. Bütün mesajı şifrelemek yerine mesajın hashi şifrelenerek alandan kazanılabilir.


